import React from 'react';

import AlertsChart from './components/AlertsChart'
import './App.css';
import DestinationPortChart from './components/DestinationPortChart'
import SeverityChart from './components/SeverityChart'
import ProtocolChart from './components/ProtocolChart'
import TrafficChart from './components/TrafficChart';

function App() {
  return (
    <div className="min-h-screen p-5 bg-primary text-textColor">
      <h1 className="text-3xl font-bold mb-5 text-center">Security Dashboard</h1>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-5">
       
       <AlertsChart/>
       <DestinationPortChart/>
<ProtocolChart/>
<SeverityChart />
<TrafficChart/>
       

      </div>
    </div>
  );
}

export default App;
