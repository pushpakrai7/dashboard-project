import React from 'react';
import { Line } from 'react-chartjs-2';
import 'chart.js/auto';
import data from '../assets/eve.json';
import '../App.css'


const TrafficChart = () => {
  const timestamps = data.map(entry => new Date(entry.timestamp).toLocaleTimeString());
  const srcIps = data.map(entry => entry.src_ip);

  const chartData = {
    labels: timestamps,
    datasets: [
      {
        label: 'Traffic by Source IP',
        data: srcIps.map(ip => ip.length),
        borderColor: 'rgba(54, 162, 235, 1)',
        backgroundColor: 'rgba(54, 162, 235, 0.2)',
        fill: true,
      },
    ],
  };

  const options = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      x: {
        ticks: { color: '#ffffff' },
      },
      y: {
        ticks: { color: '#ffffff' },
      },
    },
    plugins: {
      legend: { labels: { color: '#ffffff' } },
    },
  };

  return (
    <div className="bg-secondary p-4 rounded-lg shadow-lg col-span-1 md:col-span-2 lg:col-span-3">
      <h2 className="text-xl mb-2 text-white">Traffic by Source IP</h2>
      <div className="chart-container">
        <Line data={chartData} options={options} />
      </div>
    </div>
  );
};

export default TrafficChart;
