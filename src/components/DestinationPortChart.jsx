import React from 'react';
import { Bar } from 'react-chartjs-2';
import 'chart.js/auto';
import '../App.css';

import data from '../assets/eve.json';

const DestinationPortChart = () => {
  const portCounts = data.reduce((acc, entry) => {
    const port = entry.dest_port;
    acc[port] = (acc[port] || 0) + 1;
    return acc;
  }, {});

  const chartData = {
    labels: Object.keys(portCounts),
    datasets: [
      {
        label: 'Destination Ports',
        data: Object.values(portCounts),
        backgroundColor: 'rgba(153, 102, 255, 0.5)',
        borderColor: 'rgba(153, 102, 255, 1)',
        borderWidth: 1,
      },
    ],
  };

  const options = {
    indexAxis: 'y', // This makes the chart horizontal
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      x: {
        ticks: { color: '#ffffff' },
      },
      y: {
        ticks: { color: '#ffffff' },
      },
    },
    plugins: {
      legend: { labels: { color: '#ffffff' } },
    },
  };

  return (
    <div className="bg-secondary p-4 rounded-lg shadow-lg col-span-1 w-full">
      <h2 className="text-xl mb-2 text-white">Destination Ports Distribution</h2>
      <div className="chart-container">
        <Bar data={chartData} options={options} />
      </div>
    </div>
  );
};

export default DestinationPortChart;
