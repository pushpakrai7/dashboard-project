import React from 'react';
import { Bar } from 'react-chartjs-2';
import 'chart.js/auto';
import data from '../assets/eve.json';
import '../App.css'


const AlertsChart = () => {
  const timestamps = data.map(entry => new Date(entry.timestamp).toLocaleTimeString());
  const severities = data.map(entry => entry.alert ? entry.alert.severity : 0);

  const chartData = {
    labels: timestamps,
    datasets: [
      {
        label: 'Alerts Over Time',
        data: severities,
        backgroundColor: 'rgba(16, 185, 129, 0.5)',
        borderColor: 'rgba(16, 185, 129, 1)',
        borderWidth: 1,
      },
    ],
  };

  const options = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      x: {
        ticks: { color: '#ffffff' },
      },
      y: {
        ticks: { color: '#ffffff' },
      },
    },
    plugins: {
      legend: { labels: { color: '#ffffff' } },
    },
  };

  return (
    <div className="bg-secondary p-4 rounded-lg shadow-lg col-span-1 md:col-span-2 lg:col-span-3">
      <h2 className="text-xl mb-2 text-white">Alerts Over Time</h2>
      <div className="chart-container">
        <Bar data={chartData} options={options} />
      </div>
    </div>
  );
};

export default AlertsChart;
