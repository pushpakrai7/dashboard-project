

# Security Dashboard

## Overview

The Security Dashboard is a web application designed to provide users with visual insights into security-related data. It offers various charts and graphs to help users understand trends and patterns in security events.

## Features

- **Alerts Over Time**: Visualizes the distribution of alerts over time.
- **Traffic by Source IP**: Displays traffic data categorized by the source IP.
- **Alert Severity Distribution**: Illustrates the distribution of alert severities.
- **Protocol Distribution**: Shows the distribution of protocols used in the network traffic.
- **Destination Ports Distribution**: Visualizes the distribution of destination ports.

## Deployment

The Security Dashboard is deployed and accessible at [https://dashboard-8twiq7re5-pushpakrai1607s-projects.vercel.app/](https://dashboard-8twiq7re5-pushpakrai1607s-projects.vercel.app/).

## Technologies Used

- **React**: JavaScript library for building user interfaces.
- **Chart.js**: JavaScript library for creating interactive charts and graphs.
- **Tailwind CSS**: Utility-first CSS framework for styling.
- **Vite**: Next-generation frontend tooling for modern web development.

## Installation

1. Clone the repository:

```bash
git clone https://github.com/your-username/security-dashboard.git
```

2. Navigate to the project directory:

```bash
cd security-dashboard
```

3. Install dependencies:

```bash
npm install
```

4. Run the development server:

```bash
npm run dev
```

5. Open your browser and go to `http://localhost:3000` to view the application.

## Usage

- Explore the different charts by navigating through the tabs or sections.
- Hover over data points to view additional information.
- Click on legends to toggle visibility of data sets in the charts.

## Contributing

Contributions are welcome! If you'd like to contribute to this project, please follow these steps:

1. Fork the repository.
2. Create a new branch (`git checkout -b feature/my-feature`).
3. Make your changes.
4. Commit your changes (`git commit -am 'Add new feature'`).
5. Push to the branch (`git push origin feature/my-feature`).
6. Create a new Pull Request.

## License

This project is licensed under the [MIT License](LICENSE).

## Acknowledgements

- Data provided by [WiJungle](https://www.wijungle.com/).
- Inspired by real-world security dashboards used in network monitoring and threat detection.
